package anosym.json.serialization;

import java.io.IOException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

import anosym.json.serialization.provider.ObjectMapperFactory;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 14, 2018, 9:02:40 PM
 */
public class CaseInsensitiveEnumSerializationTest {

  @Test
  public void testDeserialization_caseInsensitive() throws IOException {
    final String json = "{\"caseType1\": \"CASE_INSENSITIVE\", \"caseType2\":\"case_sensitive\"}";
    final CaseType caseType = ObjectMapperFactory.getObjectMapper().readValue(json, CaseType.class);
    assertThat(caseType, equalTo(new CaseType(Case.Case_Insensitive, Case.Case_Sensitive)));
  }

  enum Case {

    Case_Insensitive,
    Case_Sensitive;

  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  static class CaseType {

    private Case caseType1;

    private Case caseType2;

  }

}
