package anosym.json.serialization.datetime;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import anosym.json.serialization.provider.ObjectMapperFactory;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 18, 2017, 5:42:23 PM
 */
public class ZonedDateTimeSerializerTest {

  private final ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();

  @Test
  public void verifyZonedDateSerialization() throws JsonProcessingException {
    final ZoneId zoneId = ZoneOffset.ofHours(2);
    final ZonedDateTime dateTime = ZonedDateTime.of(2016, 02, 18, 18, 0, 0, 0, zoneId);
    final String expectedStr = "\"2016-02-18T18:00:00.000+0200\"";
    final String actualFormattedValue = objectMapper.writeValueAsString(dateTime);
    assertThat(actualFormattedValue, is(expectedStr));
  }

}
