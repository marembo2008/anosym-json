package anosym.json.serialization.datetime;

import java.time.OffsetDateTime;

import org.junit.jupiter.api.Test;

import anosym.json.serialization.provider.ObjectMapperFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static java.time.Instant.ofEpochMilli;
import static java.time.OffsetDateTime.ofInstant;
import static java.time.ZoneOffset.ofHours;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author marembo
 */
public class OffsetDateTimeDeserializerTest {

    private final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

    @Test
    public void testDeserialize() throws Exception {
        final var expected = new Example("12345", ofInstant(ofEpochMilli(1648051389000L), ofHours(2)));
        final var json = "{\"id\": \"12345\", \"dateTime\":\"2022-03-23T18:03:09+0200\"}";
        final var actual = mapper.readValue(json, Example.class);
        assertThat(actual, is(expected));
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class Example {

        private String id;

        private OffsetDateTime dateTime;
    }

}
