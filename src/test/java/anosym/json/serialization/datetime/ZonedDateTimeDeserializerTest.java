package anosym.json.serialization.datetime;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import anosym.json.serialization.provider.ObjectMapperFactory;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 18, 2017, 5:42:23 PM
 */
public class ZonedDateTimeDeserializerTest {

  private final ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();

  @Test
  public void verifyZonedDateSerialization() throws JsonProcessingException, IOException {
    final ZoneId zoneId = ZoneOffset.ofHours(2);
    final ZonedDateTime expectedDateTime = ZonedDateTime.of(2016, 02, 18, 18, 0, 0, 0, zoneId);
    final String dateTimeJson = "\"2016-02-18 18:00:00.000+0200\"";

    final ZonedDateTime actualDateTime = objectMapper.readValue(dateTimeJson, ZonedDateTime.class);
    assertThat(actualDateTime, is(expectedDateTime));
  }

  @Test
  public void verifyZonedDateSerialization_legacyUtc() throws JsonProcessingException, IOException {
    final ZoneId zoneId = ZoneOffset.ofHours(0);
    final ZonedDateTime expectedDateTime = ZonedDateTime.of(2016, 02, 18, 18, 0, 0, 0, zoneId);
    final String dateTimeJson = "\"2016-02-18 18:00:00Z\"";
    final ZonedDateTime actualDateTime = objectMapper.readValue(dateTimeJson, ZonedDateTime.class);
    assertThat(actualDateTime, is(expectedDateTime));
  }

}
