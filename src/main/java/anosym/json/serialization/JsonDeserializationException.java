package anosym.json.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.annotation.Nonnull;

import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 2, 2015, 6:58:12 PM
 */
public class JsonDeserializationException extends JsonProcessingException {

    public JsonDeserializationException(@Nonnull final String messageFormat, @Nonnull final Object... args) {
        super(format(messageFormat, args));
    }
}
