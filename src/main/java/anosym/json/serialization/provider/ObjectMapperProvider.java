package anosym.json.serialization.provider;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Produces;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 19, 2017, 10:54:30 PM
 */
@ApplicationScoped
public class ObjectMapperProvider {

    private static final ObjectMapper OBJECT_MAPPER = ObjectMapperFactory.getObjectMapper();

    @Nonnull
    @Produces
    @Dependent //important, it cannot be proxied cause of final methods
    public ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

}
