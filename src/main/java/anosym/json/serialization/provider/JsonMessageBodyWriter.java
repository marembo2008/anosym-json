package anosym.json.serialization.provider;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 19, 2015, 11:56:13 PM
 */
@Provider
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
public class JsonMessageBodyWriter implements MessageBodyWriter {

    private static final ObjectMapper OBJECT_MAPPER = ObjectMapperFactory.getObjectMapper();

    @Override
    public boolean isWriteable(@Nonnull final Class type,
            @Nonnull final Type genericType,
            @Nonnull final Annotation[] annotations,
            @Nonnull final MediaType mediaType) {
        requireNonNull(mediaType, "The mediaType must not be null");

        return MediaType.APPLICATION_JSON_TYPE.isCompatible(mediaType);
    }

    @Override
    public long getSize(@Nonnull final Object object,
            @Nonnull final Class type,
            @Nonnull final Type genericType,
            @Nonnull final Annotation[] annotations,
            @Nonnull final MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(@Nullable final Object value,
            @Nonnull final Class type,
            @Nonnull final Type genericType,
            @Nonnull final Annotation[] annotations,
            @Nonnull final MediaType mediaType,
            @Nonnull final MultivaluedMap httpHeaders,
            @Nonnull final OutputStream entityStream) throws IOException, WebApplicationException {
        if (value == null) {
            return;
        }

        final String json = OBJECT_MAPPER.writeValueAsString(value);
        entityStream.write(json.getBytes());
    }

}
