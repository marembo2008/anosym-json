package anosym.json.serialization.provider;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.Provider;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 19, 2015, 11:56:13 PM
 */
@Provider
@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
public class JsonMessageBodyReader implements MessageBodyReader {

  private static final ObjectMapper OBJECT_MAPPER = ObjectMapperFactory.getObjectMapper();

  @Override
  public boolean isReadable(@Nonnull final Class type,
                            @Nonnull final Type genericType,
                            @Nonnull final Annotation[] annotations,
                            @Nonnull final MediaType mediaType) {
    requireNonNull(mediaType, "The mediaType must not be null");

    return MediaType.APPLICATION_JSON_TYPE.isCompatible(mediaType);
  }

  @Override
  public Object readFrom(@Nonnull final Class type,
                         @Nonnull final Type genericType,
                         @Nonnull final Annotation[] annotations,
                         @Nonnull final MediaType mediaType,
                         @Nonnull final MultivaluedMap httpHeaders,
                         @Nonnull final InputStream entityStream) throws IOException, WebApplicationException {
    requireNonNull(genericType, "The genericType must not be null");
    requireNonNull(entityStream, "The entityStream must not be null");

    final JavaType javaType = TypeFactory.defaultInstance().constructType(genericType);
    return OBJECT_MAPPER.readValue(entityStream, javaType);
  }

}
