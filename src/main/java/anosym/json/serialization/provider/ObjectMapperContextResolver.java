package anosym.json.serialization.provider;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.ws.rs.ext.ContextResolver;
import jakarta.ws.rs.ext.Provider;

import static com.google.common.base.Preconditions.checkState;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 3, 2018, 11:52:14 PM
 */
@Provider
public class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {

  @Override
  public ObjectMapper getContext(@Nonnull final Class<?> type) {
    checkState(ObjectMapper.class.isAssignableFrom(type), "Not an ObjectMapper type");

    return ObjectMapperFactory.getObjectMapper();
  }

}
