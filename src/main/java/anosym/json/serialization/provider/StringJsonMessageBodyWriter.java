package anosym.json.serialization.provider;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Strings are not properly registered by the default message body writer.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 19, 2015, 11:56:13 PM
 */
@Provider
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
public class StringJsonMessageBodyWriter implements MessageBodyWriter<String> {

  @Override
  public boolean isWriteable(@Nonnull final Class<?> type,
                             @Nonnull final Type genericType,
                             @Nonnull final Annotation[] annotations,
                             @Nonnull final MediaType mediaType) {
    requireNonNull(mediaType, "The mediaType must not be null");

    return MediaType.APPLICATION_JSON_TYPE.isCompatible(mediaType)
            && type == String.class;
  }

  @Override
  public long getSize(@Nonnull final String object,
                      @Nonnull final Class<?> type,
                      @Nonnull final Type genericType,
                      @Nonnull final Annotation[] annotations,
                      @Nonnull final MediaType mediaType) {
    return -1;
  }

  @Override
  public void writeTo(@Nullable final String value,
                      @Nonnull final Class<?> type,
                      @Nonnull final Type genericType,
                      @Nonnull final Annotation[] annotations,
                      @Nonnull final MediaType mediaType,
                      @Nonnull final MultivaluedMap<String, Object> httpHeaders,
                      @Nonnull final OutputStream entityStream) throws IOException, WebApplicationException {
    if (value == null) {
      return;
    }

    // json strings are quoted.
    final String json = format("\"%s\"", value);
    entityStream.write(json.getBytes());
  }

}
