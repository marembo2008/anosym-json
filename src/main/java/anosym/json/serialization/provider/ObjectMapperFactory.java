package anosym.json.serialization.provider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.ImmutableList;

import anosym.json.serialization.TypedJsonDeserializer;
import anosym.json.serialization.TypedJsonSerializer;

import org.atteo.classindex.ClassIndex;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 2, 2015, 7:05:25 PM
 */
@Slf4j
public class ObjectMapperFactory {

    private static final ObjectMapper OBJECT_MAPPER = initObjectMapper();

    @Nonnull
    public static ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    @Nonnull
    @SneakyThrows
    private static ObjectMapper initObjectMapper() {
        return JsonMapper.builder()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .disable(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS)
                .addModules(ObjectMapper.findModules()) // Any default modeule available
                .addModule(new JavaTimeModule()) // Does not support auto-registration?
                .addModule(customModule())
                .visibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
                .visibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
                .build();
    }

    @Nonnull
    private static SimpleModule customModule() {
        final SimpleModule module = new SimpleModule();
        addJsonDeserializers(module);
        addJsonSerializers(module);

        return module;
    }

    private static void addJsonDeserializers(@Nonnull final SimpleModule module) {
        /**
         * Using this classloader to ensure that this works properly in jakarta
         * ee, where we may have ear classloader or war classloader
         */
        final var classLoader = ObjectMapperFactory.class.getClassLoader();
        final var jsonDeserializerClasses = ImmutableList
                .copyOf(ClassIndex.getSubclasses(TypedJsonDeserializer.class, classLoader));

        log.info("jsonDeserializerClasses: {}", jsonDeserializerClasses);

        jsonDeserializerClasses.stream()
                .peek((deserializer) -> log.info("initializing object mapper deserializer: {}", deserializer))
                .map(ObjectMapperFactory::initializeInstance)
                .map(TypedJsonDeserializer.class::cast)
                .forEach((jsonDeserializer) -> module.addDeserializer(jsonDeserializer.getTyped(), jsonDeserializer));
    }

    private static void addJsonSerializers(@Nonnull final SimpleModule module) {
        /**
         * Using this classloader to ensure that this works properly in jakarta
         * ee, where we may have ear classloader or war classloader
         */
        final var classLoader = ObjectMapperFactory.class.getClassLoader();
        final var jsonSerializerClasses = ImmutableList
                .copyOf(ClassIndex.getSubclasses(TypedJsonSerializer.class, classLoader));

        log.info("jsonSerializerClasses: {}", jsonSerializerClasses);

        jsonSerializerClasses.stream()
                .peek((serializer) -> log.info("initializing object mapper serializer: {}", serializer))
                .map(ObjectMapperFactory::initializeInstance)
                .map(TypedJsonSerializer.class::cast)
                .forEach((jsonSerializer) -> module.addSerializer(jsonSerializer.getTyped(), jsonSerializer));
    }

    @Nonnull
    @SneakyThrows
    private static <T> T initializeInstance(final Class<T> clazz) {
        final var constructor = clazz.getConstructor();
        return constructor.newInstance();
    }

}
