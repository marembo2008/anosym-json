package anosym.json.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.time.Duration;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2016, 3:27:35 AM
 */
public class DurationJsonSerializer extends TypedJsonSerializer<Duration> {

    @Override
    public void serialize(@Nullable final Duration duration,
                          @Nonnull final JsonGenerator jsonGenerator,
                          @Nonnull final SerializerProvider serializers) throws IOException, JsonProcessingException {
        checkNotNull(jsonGenerator, "The jsonGenerator must not be null");
        checkNotNull(serializers, "The serializers must not be null");

        if (duration == null) {
            return;
        }

        jsonGenerator.writeNumber(duration.toMillis());
    }

}
