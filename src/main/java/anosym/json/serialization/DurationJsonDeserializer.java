package anosym.json.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.time.Duration;
import jakarta.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2016, 3:30:28 AM
 */
public class DurationJsonDeserializer extends TypedJsonDeserializer<Duration> {

    @Override
    public Duration deserialize(@Nonnull final JsonParser jsonParser, @Nonnull final DeserializationContext ctxt) throws IOException, JsonProcessingException {
        checkNotNull(jsonParser, "The jsonParser must not be null");
        checkNotNull(ctxt, "The ctxt must not be null");

        final ObjectCodec objectCodec = jsonParser.getCodec();
        final JsonNode valueNode = objectCodec.readTree(jsonParser);
        final long millis = valueNode.asLong();

        return Duration.ofMillis(millis);
    }

}
