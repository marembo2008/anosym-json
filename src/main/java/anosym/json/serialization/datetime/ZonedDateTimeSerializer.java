package anosym.json.serialization.datetime;

import java.io.IOException;
import java.io.Serializable;
import java.time.ZonedDateTime;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

import anosym.json.serialization.TypedJsonSerializer;

import static anosym.json.serialization.datetime.DateTimeFormatterUtil.WRITE_ZONED_DATE_TIME_FORMATTER;
import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 18, 2017, 2:07:31 PM
 */
public class ZonedDateTimeSerializer extends TypedJsonSerializer<ZonedDateTime> implements Serializable {

  private static final long serialVersionUID = -1749032554801429971L;

  @Override
  public void serialize(@Nullable final ZonedDateTime dateTime,
                        @Nonnull final JsonGenerator jsonGenerator,
                        @Nonnull final SerializerProvider serializers) throws IOException, JsonProcessingException {
    requireNonNull(jsonGenerator, "The jsonGenerator must not be null");
    requireNonNull(serializers, "The serializers must not be null");

    if (dateTime == null) {
      return;
    }

    jsonGenerator.writeString(dateTime.format(WRITE_ZONED_DATE_TIME_FORMATTER));
  }

}
