package anosym.json.serialization.datetime;

import static anosym.json.serialization.datetime.DateTimeFormatterUtil.READ_OFFSET_TIME_FORMATTER;
import static anosym.json.serialization.datetime.LegacyUtcZoneSanitizer.sanitizeLegacyUtc;

import java.io.IOException;
import java.time.OffsetTime;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import anosym.json.serialization.TypedJsonDeserializer;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 23, 2016, 11:07:03 PM
 */
public class OffsetTimeDeserializer extends TypedJsonDeserializer<OffsetTime> {

  @Override
  public OffsetTime deserialize(@Nonnull final JsonParser jsonParser,
                                @Nonnull final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    final ObjectCodec objectCodec = jsonParser.getCodec();
    final JsonNode dateTimeNode = objectCodec.readTree(jsonParser);
    if (dateTimeNode == null) {
      return null;
    }

    final String dateTime = sanitizeLegacyUtc(dateTimeNode.asText());
    return OffsetTime.parse(dateTime, READ_OFFSET_TIME_FORMATTER);
  }

}
