package anosym.json.serialization.datetime;

import java.time.format.DateTimeFormatter;

import static java.time.format.DateTimeFormatter.ofPattern;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 18, 2017, 5:27:50 PM
 */
public final class DateTimeFormatterUtil {

  private static final String WRITE_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

  private static final String READ_DATE_TIME_FORMAT = "yyyy-MM-dd['T'][' ']HH:mm:ss[.SSS]Z";

  private static final String WRITE_OFFSET_TIME_FORMAT = "HH:mm:ss.SSSZ";

  private static final String READ_OFFSET_TIME_FORMAT = "HH:mm:ss[.SSS]Z";

  public static final DateTimeFormatter WRITE_ZONED_DATE_TIME_FORMATTER = ofPattern(WRITE_DATE_TIME_FORMAT);

  public static final DateTimeFormatter READ_ZONED_DATE_TIME_FORMATTER = ofPattern(READ_DATE_TIME_FORMAT);

  public static final DateTimeFormatter WRITE_OFFSET_DATE_TIME_FORMATTER = ofPattern(WRITE_DATE_TIME_FORMAT);

  public static final DateTimeFormatter READ_OFFSET_DATE_TIME_FORMATTER = ofPattern(READ_DATE_TIME_FORMAT);

  public static final DateTimeFormatter WRITE_OFFSET_TIME_FORMATTER = ofPattern(WRITE_OFFSET_TIME_FORMAT);

  public static final DateTimeFormatter READ_OFFSET_TIME_FORMATTER = ofPattern(READ_OFFSET_TIME_FORMAT);

}
