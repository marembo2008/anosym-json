package anosym.json.serialization.datetime;

import java.io.IOException;
import java.time.ZonedDateTime;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import anosym.json.serialization.TypedJsonDeserializer;

import static anosym.json.serialization.datetime.DateTimeFormatterUtil.READ_ZONED_DATE_TIME_FORMATTER;
import static anosym.json.serialization.datetime.LegacyUtcZoneSanitizer.sanitizeLegacyUtc;
import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 23, 2016, 11:07:03 PM
 */
public class ZonedDateTimeDeserializer extends TypedJsonDeserializer<ZonedDateTime> {

  @Override
  public ZonedDateTime deserialize(@Nonnull final JsonParser jsonParser,
                                   @Nonnull final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    requireNonNull(jsonParser, "The jsonParser must not be null");
    requireNonNull(ctxt, "The ctxt must not be null");

    final ObjectCodec objectCodec = jsonParser.getCodec();
    final JsonNode dateTimeNode = objectCodec.readTree(jsonParser);
    if (dateTimeNode == null) {
      return null;
    }

    final String dateTime = sanitizeLegacyUtc(dateTimeNode.asText());
    return ZonedDateTime.parse(dateTime, READ_ZONED_DATE_TIME_FORMATTER);
  }

}
