package anosym.json.serialization.datetime;

import static anosym.json.serialization.datetime.DateTimeFormatterUtil.WRITE_OFFSET_DATE_TIME_FORMATTER;

import java.io.IOException;
import java.io.Serializable;
import java.time.OffsetDateTime;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

import anosym.json.serialization.TypedJsonSerializer;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 16, 2018, 2:30:27 AM
 */
public class OffsetDateTimeSerializer extends TypedJsonSerializer<OffsetDateTime> implements Serializable {

  @Override
  public void serialize(@Nullable final OffsetDateTime dateTime,
                        @Nonnull final JsonGenerator jsonGenerator,
                        @Nonnull final SerializerProvider serializers) throws IOException, JsonProcessingException {
    if (dateTime == null) {
      return;
    }

    jsonGenerator.writeString(WRITE_OFFSET_DATE_TIME_FORMATTER.format(dateTime));
  }

}
