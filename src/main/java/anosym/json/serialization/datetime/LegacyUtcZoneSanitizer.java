package anosym.json.serialization.datetime;

import jakarta.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 5, 2019, 11:25:25 PM
 */
public class LegacyUtcZoneSanitizer {

  @Nonnull
  public static String sanitizeLegacyUtc(@Nonnull final String dateTime) {
    return dateTime
            .replace("Z", "+0000")
            .replace("z", " UTC");
  }

}
