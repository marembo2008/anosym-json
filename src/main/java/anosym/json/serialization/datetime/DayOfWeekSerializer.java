package anosym.json.serialization.datetime;

import java.io.IOException;
import java.io.Serializable;
import java.time.DayOfWeek;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

import anosym.json.serialization.TypedJsonSerializer;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 16, 2018, 2:30:27 AM
 */
public class DayOfWeekSerializer extends TypedJsonSerializer<DayOfWeek> implements Serializable {

  @Override
  public void serialize(@Nullable final DayOfWeek dayOfWeek,
                        @Nonnull final JsonGenerator jsonGenerator,
                        @Nonnull final SerializerProvider serializers) throws IOException, JsonProcessingException {
    if (dayOfWeek == null) {
      return;
    }

    final String name = dayOfWeek.name().toLowerCase();
    final String capName = name.substring(0, 1).toUpperCase() + name.substring(1);
    jsonGenerator.writeString(capName);
  }

}
