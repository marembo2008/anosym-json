package anosym.json.serialization.datetime;

import java.io.IOException;
import java.time.DayOfWeek;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import anosym.json.serialization.TypedJsonDeserializer;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 23, 2016, 11:07:03 PM
 */
public class DayOfWeekDeserializer extends TypedJsonDeserializer<DayOfWeek> {

  @Override
  public DayOfWeek deserialize(@Nonnull final JsonParser jsonParser,
                               @Nonnull final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    final ObjectCodec objectCodec = jsonParser.getCodec();
    final JsonNode dayOfWeekNode = objectCodec.readTree(jsonParser);
    if (dayOfWeekNode == null) {
      return null;
    }

    final String dayOfWeekName = dayOfWeekNode.asText();
    return DayOfWeek.valueOf(dayOfWeekName.toUpperCase());
  }

}
