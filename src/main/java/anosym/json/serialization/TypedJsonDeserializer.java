package anosym.json.serialization;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.reflect.TypeToken;
import jakarta.annotation.Nonnull;
import org.atteo.classindex.IndexSubclasses;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 23, 2016, 11:24:59 PM
 */
@IndexSubclasses
public abstract class TypedJsonDeserializer<T> extends JsonDeserializer<T> {

    private final Class<?> typed;

    public TypedJsonDeserializer() {
        this.typed = new TypeToken<T>(getClass()) {
        }.getRawType();
    }

    @Nonnull
    public Class<?> getTyped() {
        return typed;
    }

}
